package examenUT4V2_CeliaMu�oz;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Pattern;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;

public class examenUT4V2 {

	public static void main(String[] args) {

		ArrayList<Precipitacion> precipitaciones = obtenerPrecipitaciones();
		precipitaciones=eliminaDiasNublados(precipitaciones);

		//EXPRESIONES REGULARES

		//Crea una expresi�n regular para validar el nombre de una clase Java de acuerdo al estilo CamelCase
		String prueba1="([A-Z][a-z0-9]+)+";
	    String cadena1="TrabajadorAutonomo";
	    System.out.println(validarExpresionesRegulares(cadena1, prueba1));

	    //Crea una expresi�n regular para validar n�meros con 2 decimales.
		String prueba2="(^-[0-9]+[\\.|,][0-9]*)|([0-9]*[\\.|,][0-9]+)|([0-9]+)";
        String cadena2="12,00";
        System.out.println(validarExpresionesRegulares(cadena2, prueba2));

	    // Crea una expresi�n regular para validar palabras en min�scula de longitud par
		String prueba3="([a-z]{2,})";
        String cadena3="coraza";
        System.out.println(validarExpresionesRegulares(cadena3, prueba3));

        //Crea una expresi�n regular para validar n�meros binarios que empiecen con 0 y que tengan una longitud par
		String prueba4="^[0-1]{1,}$";
        String cadena4="00";
        System.out.println(validarExpresionesRegulares(cadena4, prueba4));
	}

	public static ArrayList obtenerPrecipitaciones() {

		boolean saltar = false;

		ArrayList<Precipitacion>precipitaciones = new ArrayList<Precipitacion>();
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			Document documentJDOM = builder.parse(new File("precipitaciones.xml"));

			NodeList nodosPrecipitaciones = documentJDOM.getElementsByTagName("precipitaciones");


			for (int i=0;i<nodosPrecipitaciones.getLength();i++) {
				Node nodo = nodosPrecipitaciones.item(i);
				if (nodo.getNodeType() == Node.ELEMENT_NODE) {
                    // Lo transformo a Element
                    Element e = (Element) nodo;
                    // Obtengo sus hijos
                    NodeList hijos = e.getChildNodes();
                    // Recorro sus hijos
                    Precipitacion p=null;
                    for (int j = 0; j < hijos.getLength(); j++) {
                        // Obtengo al hijo actual
                    	saltar = false;
                        Node hijo = hijos.item(j);
                        NodeList nietos = hijo.getChildNodes();
                        p = new Precipitacion();
                        for (int h = 0; h < nietos.getLength(); h++) {
                        	Node nieto = nietos.item(h);
                        	if (saltar) {
                        		continue;
                        	}
	                        // Compruebo si es un nodo
	                        if (nieto.getNodeType() == Node.ELEMENT_NODE) {
	                            // Muestro el contenido
	                        	if (nieto.getNodeName()=="litros") {
	                        		if (nieto.getTextContent()=="0.0"){
	                        			saltar = true;
	                        		}
	                        		else {
	                        			p.setLitros(Double.parseDouble(nieto.getTextContent()));
	                        		}
	                        	}
	                        	if (nieto.getNodeName()=="temp_max") {
	                        		p.setTemp_max(Double.parseDouble(nieto.getTextContent()));
	                        	}
	                        	if (nieto.getNodeName()=="temp_min") {
	                        		p.setTemp_min(Double.parseDouble(nieto.getTextContent()));
	                        	}
	                        	if (nieto.getNodeName()=="viento") {
	                        		p.setViento(Double.parseDouble(nieto.getTextContent()));
	                        	}
	                        	if (nieto.getNodeName()=="tiempo") {
	                        		p.setTiempo(nieto.getTextContent());
	                        	}
                        }
                        }
                    }
                    precipitaciones.add(p);
				}

			}
			}

		catch (Exception e) {
			e.printStackTrace();
		}

		return precipitaciones;

	}


	public static ArrayList<Precipitacion> eliminaDiasNublados(ArrayList<Precipitacion> precipitaciones){
		Iterator<Precipitacion> it = precipitaciones.iterator();
		while(it.hasNext()){
			Precipitacion p = it.next();
			if (p.getTiempo()=="fog"){
				it.remove();
			}
		}

		return precipitaciones;
		}


	public static boolean validarExpresionesRegulares(String cadena1, String regex) {
		return Pattern.matches(regex, cadena1);
	}


}
