package examenUT4V2_CeliaMuņoz;

public class Precipitacion {

	//ATRIBUTOS

	String fecha;
	double litros;
	double temp_max;
	double temp_min;
	double viento;
	String tiempo;

	//CONSTRUCTOR

	public Precipitacion() {
		this.fecha=fecha;
		this.litros=litros;
		this.temp_max=temp_max;
		this.temp_min=temp_min;
		this.viento=viento;
		this.tiempo=tiempo;
	}

	//GETTERS Y SETTERS

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public double getLitros() {
		return litros;
	}

	public void setLitros(double litros) {
		this.litros = litros;
	}

	public double getTemp_max() {
		return temp_max;
	}

	public void setTemp_max(double temp_max) {
		this.temp_max = temp_max;
	}

	public double getTemp_min() {
		return temp_min;
	}

	public void setTemp_min(double temp_min) {
		this.temp_min = temp_min;
	}

	public double getViento() {
		return viento;
	}

	public void setViento(double viento) {
		this.viento = viento;
	}

	public String getTiempo() {
		return tiempo;
	}

	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}




}
